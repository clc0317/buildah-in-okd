FROM fedora:29
LABEL maintainer='Chris Collins <christopher.collins@duke.edu>'


RUN dnf install -y buildah \
      && dnf clean all \
      && rm -rf /var/cache/yum

# Need to create a homedir for the user
# or Buildah doesn't like the missing storage.conf
#
ENV HOME=/home/user
RUN mkdir -p ${HOME} \
      && chgrp -R 0 ${HOME} \
      && chmod -R g=u ${HOME}

WORKDIR ${HOME}

# We need to add a way to determine the user from the UID
# https://docs.okd.io/3.11/creating_images/guidelines.html#openshift-specific-guidelines
#
RUN chmod g=u /etc/passwd

# This is a script to set the passwd values and then execute buildah
# Make sure to set the "command:" correctly in the OpenShift/Kubernetes pod
# 
# Using "ENTRYPOINT" so CMD/args can be used to pass options
COPY init.sh ${HOME}/init.sh
ENTRYPOINT [ "./init.sh" ]

# This will be a random UID when run in OKD:
# https://docs.okd.io/3.11/creating_images/guidelines.html#openshift-specific-guidelines
#
USER 1001

