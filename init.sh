#!/usr/bin/env bash
# https://docs.okd.io/3.11/creating_images/guidelines.html#openshift-specific-guidelines

if ! whoami &> /dev/null; then
  if [ -w /etc/passwd ]; then
    echo "${USER_NAME:-default}:x:$(id -u):0:${USER_NAME:-default} user:${HOME}:/sbin/nologin" >> /etc/passwd
  fi
fi

exec buildah bud "$@"
