# Buildah in OKD

Attempting to run [Buildah](https://buildah.io/) in [OKD](https://www.okd.io/), to build container images.

## Goals

1.  Build container images inside of a pod running in OKD
2.  Build without any special capabilities, unprivileged, and without root
3.  Build "FROM" any parent image
4.  Support builds via `buildah bud`

__Bonus:__ Support builds *not* using `buildah bud`, to allow folks to use their own tools.


## Process

### Create a Buildah image
Build a container image with Buildah inside of it.  The `Dockerfile` in this repo is the Buildah container image, "FROM" the Fedora base image. Use this image in your pod (you'll need to put it in a registry somewhere most likely.)

```
$ podman build -t buildah:1.0 .
$ podman tag buildah:1.0 <your registry>/<your repository>/buildah:1.0
```

For ease of use, you may also just pull `docker.io/clcollins/buildah:1.0`:

```
$ oc import-image docker.io/clcollins/buildah:1.0 --confirm
$ oc tag --scheduled=true docker.io/clcollins/buildah:1.0 buildah:1.0


$ oc get image-stream buildah
NAME      DOCKER REPO                                               TAGS      UPDATED
buildah   docker-registry.default.svc:5000/buildah-in-okd/buildah   1.0       56 seconds ago
```

### Create a Dockerfile configMap to use with OKD

The idea is to build a Docker image inside the Buildah image created above.  We can provide a Dockerfile as a configMap to the pod attempting the build.

Create a Dockerfile to test with:

```
# Dockerfile-ubuntu
FROM ubuntu:latest

# Using an apt update to test if we can write inside the container later
RUN apt-get update \
  && apt-get upgrade -y

CMD ['echo', 'Hello World!']
```

And create a configMap:

```
$ oc create configmap dockerfile --from-file=Dockerfile-ubuntu

$ oc oc describe configmap dockerfile
Name:         dockerfile
Namespace:    buildah-in-okd
Labels:       <none>
Annotations:  <none>

Data
====
Dockerfile-ubuntu:
----
# Dockerfile-ubuntu
FROM ubuntu:latest

# Using an apt update to test if we can write inside the container later
RUN apt-get update \
  && apt-get upgrade -y

CMD ['echo', 'Hello World!']


Events:  <none>
```

### Create a Kubernetes Job to run the build

Create job that will run the buildah container and try to build the image from the Dockerfile-ubuntu configMap.  For ease of use, you can just create the job from the "buildJob.yaml" file included in this repo:

```
oc create -f buildJob.yaml
job.batch/buildah-in-okd created
```

Or write and create your own:

```
apiVersion: batch/v1
kind: Job
metadata:
  name: buildah-in-okd
spec:
  backoffLimit: 5
  activeDeadlineSeconds: 100
  template:
    metadata:
      annotations:
        alpha.image.policy.openshift.io/resolve-names: '*'
    spec:
      containers:
      - name: buildah
        image: buildah:1.0
        imagePullPolicy: Always
        command:
        - "./init.sh"
        args:
        - "-t testimage:latest"
        - "."
        volumeMounts:
        - name: dockerfile-vol
          mountPath: /home/user/Dockerfile
      restartPolicy: Never
      volumes:
      - name: dockerfile-vol
        configMap:
          name: dockerfile
          items:
          - key: Dockerfile-ubuntu
            path: Dockerfile
```

## Validation

The Job should create a pod that attempts to run Buildah with the arguments passed to it, though the init.sh for the passwd change.  Find the pod created by the job and see if it was successful.  If not, troubleshoot by looking at the logs, and with the `oc debug` command to test from inside the container:

```
$ oc get jobs
NAME             DESIRED   SUCCESSFUL   AGE
buildah-in-okd   1         0            17m
$ oc get pods
NAME                   READY     STATUS    RESTARTS   AGE
buildah-in-okd-9jqjl   0/1       Error     0          16m
buildah-in-okd-ggfsg   0/1       Error     0          17m
buildah-in-okd-m76qh   0/1       Error     0          17m
buildah-in-okd-m7hr4   0/1       Error     0          17m
$ oc logs buildah-in-okd-9jqjl
time="2019-01-30T17:52:43Z" level=error msg="error reading allowed ID mappings: error reading subuid mappings for user \"default\" and subgid mappings for group \"default\": No subuid ranges found for user \"default\" in /etc/subuid"
$ oc debug buildah-in-okd-9jqjl
...

```

## Issues

__Unable to Pull Image__

Error:

The job pod is unable to find the buildah:1.0 image

Fix:

Kubernetes Job spec is not OKD/OpenShift-aware, and doesn't know how to pull image stream images.  Add an annotation to the job .spec.template.metadata:

`alpha.image.policy.openshift.io/resolve-names: '*'`

__Unknown Userid__

Error:

`time="2019-01-30T17:24:21Z" level=error msg="error determining current user: user: unknown userid 1001400000"`

Fix:

Since OKD uses a random, unprivileged UID, the passwd file needs to be dynamically updated when run.  The init.sh file in this repo wraps the buildah command with the passwd update, and then executes buildah with any arguments passed to init.sh (eg: `-t testimage:latest .`)

See: [OpenShift-specific image guidelines](https://docs.okd.io/3.11/creating_images/guidelines.html#openshift-specific-guidelines)

__Error Reading Subuid__

Error:

`time="2019-01-30T17:51:53Z" level=error msg="error reading allowed ID mappings: error reading subuid mappings for user \"default\" and subgid mappings for group \"default\": No subuid ranges found for user \"default\" in /etc/subuid"`

Fix:

*Unknown at this time...*

__Hard-coded "buildah bud" entrypoint__

Error:

One of the Goals listed above is to allow Buildah to be run however, but the init.sh hard-codes "buildah bud".

Fix:

Maybe have the init.sh `exec $@` and pass in the full Buildah command. That seems to work for things like tini-init.


## Running locally with podman

The example above assumes OKD/OpenShift will be running the Buildah/Fedora image, but you can recreate this locally with podman:

```
podman run -v $(pwd)/Dockerfile-ubuntu:/home/user/Dockerfile \
           -it docker.io/clcollins/buildah:1.0 -t testimage:latest .
```
